interface ObjectConstructor {
  keys<Type extends Record<Key, unknown>, Key extends keyof Type>(
    object: Type,
  ): Key[];
}
