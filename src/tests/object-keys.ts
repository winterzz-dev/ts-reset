import { doNotExecute, Equal, Expect } from "./utils";

type Action = "read" | "edit" | "execute";

const ALLOWED_ACTIONS_MAP: Record<Action, boolean> = {
  read: true,
  edit: false,
  execute: false,
};

doNotExecute(() => {
  const keys = Object.keys(ALLOWED_ACTIONS_MAP);

  type tests = [Expect<Equal<typeof keys, Action[]>>];
});

doNotExecute(() => {
  const keys = Object.keys(ALLOWED_ACTIONS_MAP);

  // @ts-expect-error
  type tests = [Expect<Equal<typeof keys, string[]>>];
});
